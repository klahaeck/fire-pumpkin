$(document).ready(function() {

  function getStatus(state) {
    if (state) {
      return 'success';
    } else {
      return 'warning';
    }
  }

  $('#activate').on('click', function(e) {
    e.preventDefault();
    var url = this.href;
    $.ajax({
      type: 'GET',
      url: url,
      success: function(data) {}
    });
  });

  $('.config-toggle').on('click', function(e) {
    e.preventDefault();
    var url = this.href;
    var param = $(this).data('param');
    var that = this;
    $.ajax({
      type: 'POST',
      url: url,
      data: {config: param},
      success: function(data) {
        // console.log(data);
        var state = getStatus(data[param + '_status']);
        $(that).removeClass('btn-warning btn-success');
        $(that).addClass('btn-' + state);
      }
    });
  });

});