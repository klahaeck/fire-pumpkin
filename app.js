"use strict";

// Connect to the Arduino
var serialPort = '/dev/tty.usbserial-A8008pp1';
var board = require('./firmataConnector').start(serialPort);

var twitter = require('ntwitter');
var twit = new twitter({
  consumer_key: process.env.FIREPUMPKIN_TWITTER_KEY,
  consumer_secret: process.env.FIREPUMPKIN_TWITTER_SECRET,
  access_token_key: process.env.FIREPUMPKIN_TWITTER_TOKEN_KEY,
  access_token_secret: process.env.FIREPUMPKIN_TWITTER_TOKEN_SECRET
});

var play = require('play');

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');

var app = express();


// PUMPKIN CONFIG
var twitterOn = true;
var eyesOn = true;
var audioOn = true;
var flameOn = true;

var hashtag = '#fireball';
var audioFile = 'Devil_Laugh.mp3';
var boardConnected = false;
var pinEyeLeft = 6;
var pinEyeRight = 7;
var motorPin = 2;

board.on('connection', function () {
  boardConnected = true;
  board.pinMode(motorPin, board.OUTPUT);
  board.digitalWrite(pinEyeLeft, board.LOW);
  board.digitalWrite(pinEyeRight, board.LOW);
  board.digitalWrite(motorPin, board.HIGH);
});

twit.stream('statuses/filter', { track: [hashtag] }, function(stream) {
  stream.on('data', function(tweet) {
    if (tweet.text.match(hashtag)) {
      if (twitterOn) {
        activatePumpkin();
        console.log('tweet');
      }
    }
  });
});

var isActive = false;
function activatePumpkin() {
  if (isActive === false && (audioOn || eyesOn || fireOn)) {
    isActive = true;
    if (audioOn) {
      playSound();
      console.log('playing audio');
    }
    setTimeout(function() {
      if (eyesOn && boardConnected) {
        blinkEyes();
        console.log('blinking eyes');
      }
      if (flameOn && boardConnected) {
        breathFire();
        console.log('breathing fire');
      }
    }, 1000);
    setTimeout(function() {
      isActive = false;
    }, 10000);
  }
}

function blinkEyes() {
  var leftOn = false;
  var rightOn = false;

  var eyeLeft = setInterval(function () {
    if (leftOn) {
      board.digitalWrite(pinEyeLeft, board.LOW);
      leftOn = false;
    } else {
      board.digitalWrite(pinEyeLeft, board.HIGH);
      leftOn = true;
    }
  }, 50);

  var eyeRight = setInterval(function () {
    if (rightOn) {
      board.digitalWrite(pinEyeRight, board.LOW);
      rightOn = false;
    } else {
      board.digitalWrite(pinEyeRight, board.HIGH);
      rightOn = true;
    }
  }, 50);

  setTimeout(function() {
    clearInterval(eyeLeft);
    clearInterval(eyeRight);
    board.digitalWrite(pinEyeLeft, board.LOW);
    leftOn = false;
    board.digitalWrite(pinEyeRight, board.LOW);
    rightOn = false;
  }, 7000);
}

function playSound() {
  play.sound('./audio/' + audioFile);
}

function breathFire() {
  setTimeout(function() {
    board.digitalWrite(motorPin, board.LOW);
  }, 3500);
  setTimeout(function() {
    board.digitalWrite(motorPin, board.HIGH);
  }, 3700);
}

function getStatus(state) {
  if (state) {
    return 'success';
  } else {
    return 'warning';
  }
}

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function() {
  app.use(express.errorHandler());
});

app.get('/admin', function(req, res) {
  res.render('index', {
    twitter_status: twitterOn, twitter_button: getStatus(twitterOn),
    eyes_status: eyesOn, eyes_button: getStatus(eyesOn),
    audio_status: eyesOn, audio_button: getStatus(audioOn),
    flame_status: flameOn, flame_button: getStatus(flameOn)
  });
});

app.get('/activate', function(req, res) {
  activatePumpkin();
  res.send(200);
});

app.post('/config', function(req, res) {
  if (req.param('config') === 'twitter') {
    twitterOn = !twitterOn;
  }
  if (req.param('config') === 'eyes') {
    eyesOn = !eyesOn;
  }
  if (req.param('config') === 'audio') {
    audioOn = !audioOn;
  }
  if (req.param('config') === 'flame') {
    flameOn = !flameOn;
  }
  console.log('twitter:', twitterOn, ' eyes:', eyesOn, ' audio:', audioOn, ' flame:', flameOn);
  res.json({ twitter_status: twitterOn, eyes_status: eyesOn, audio_status: audioOn, flame_status: flameOn });
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Fire Pumpkin web server listening on port " + app.get('port'));
});
